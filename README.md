#🐺Olá, futuro matilheiro ;)

---

Se você está aqui, quer dizer que gostamos de você e agora queremos saber como você lida com código.

Preparamos um teste simples e, antes de começar, já tenha em mente o que e como vamos avaliar.

## O que:
1. Consistência do HTML, CSS e JS
2. Consistência entre navegadores (IE, Firefox, Chrome - última versão)
3. Fidelidade ao layout
4. Instruções no Readme
5. Qualidade dos commits

---

## Como:

1. Após a entrega, iremos rodar seu projeto em nossas máquinas e, para isso, precisamos de todas as instruções necessárias nesse Readme.

2. Caso ele rode perfeitamente, iremos olhar seus commits e seu código para entendermos melhor como você pensou em solucionar o problema dado.
	- _*aqui é interessante ter comentários no código para nos ajudar com esse processo_

3. E, por último, iremos fazer uma call com você para conversarmos sobre seu código.

---

## Desafio:

###Para esse desafio, queremos que você desenvolva o front-end do seguinte layout: [LAL - Influencers LP](https://app.zeplin.io/project/5c057e656f17897930e3016d)

P.s.: os arquivos de fonts se encontram nesse repositório no arquivo **fonts.zip**

Qualquer dúvida, mandar email para andrea@matilha.digital

---

## Instruções:

####Espaço para adicionar instruções de como rodar o projeto